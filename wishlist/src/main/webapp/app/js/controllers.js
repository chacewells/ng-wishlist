angular.module('familyWishlistApp.controllers', [])

.controller('WishListCtrl', ['$scope', '$http', function ($scope, $http) {
	$http.get('../rest/person/list').success(function (data) {
		$scope.people = data;
	});
	$scope.orderProp = "firstName";
	$scope.filterProp = "firstName";
}])

.controller('PersonCtrl', ['$scope', '$http', '$routeParams', function ($scope, $http, $routeParams) {
	$http.get('../rest/person/' + $routeParams.personId).success(function (person) {
		$scope.info = 'Wishlist info coming for ' + person.firstName + ' ' + person.lastName;
	});
}]);