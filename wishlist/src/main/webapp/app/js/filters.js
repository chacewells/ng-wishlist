angular.module('familyWishlistApp.filters', [])

.filter('makeUppercase', function () {
	return function (item) {
		return item.toUpperCase();
	};
})

.filter('filterBy', function () {
	String.prototype.startsWithIgnoreCase = function ( str ) {
		var cleanStr = str || '';
		var startsWithRegex = new RegExp('^' + cleanStr.toLowerCase());
		return startsWithRegex.test( this.toLowerCase() );
	};

	function propStartsWith ( prop ) {
		return function (items, filterString) {
			var filtered = [];
			var cleanFilterString = filterString || '';
			
			for (var i in items) {
				var item = items[i];
				if ( item[prop].startsWithIgnoreCase(cleanFilterString) ) {
					filtered.push(item);
				}
			}
			return filtered;
		};
	}

	return function ( items, prop, filterString ) {
		return propStartsWith(prop)(items, filterString);
	};
});