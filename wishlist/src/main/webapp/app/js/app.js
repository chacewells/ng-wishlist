angular.module('familyWishlistApp', ['ngRoute', 'familyWishlistApp.filters', 'familyWishlistApp.controllers'])

.config(['$routeProvider', function ($routeProvider) {
	$routeProvider
	.when('/wishlist/', {
		controller: 'WishListCtrl',
		templateUrl: 'partials/wishlist-all.html'
	})
	.when('/wishlist/:personId', {
		controller: 'PersonCtrl',
		templateUrl: 'partials/person.html'
	})
	.otherwise({
		redirectTo: '/wishlist/'
	});
}]);