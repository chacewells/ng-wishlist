package com.wellsfromwales.wishlist.config;

import java.util.Properties;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;

@Configuration
@PropertySource("classpath:db.properties")
public class PersistenceJpaConfig {
	
	@Autowired
	Environment environment;
	
	@Bean
	public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
		LocalContainerEntityManagerFactoryBean emf = new LocalContainerEntityManagerFactoryBean();
		emf.setDataSource(dataSource() );
		emf.setPackagesToScan("com.wellsfromwales.wishlist.model");
		emf.setJpaVendorAdapter(new HibernateJpaVendorAdapter() );
		emf.setJpaProperties(additionalProperties() );
		return emf;
	}
	
	public Properties additionalProperties() {
		Properties properties = new Properties();
		properties.setProperty("hibernate.show_sql", "true");
		properties.setProperty("hibernate.hbm2ddl.auto", "validate");
		properties.setProperty("hibernate.dialect", environment.getProperty("db.dialect") );
		return properties;
	}

	@Bean
	public DataSource dataSource() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName( environment.getProperty("db.driver") );
		dataSource.setUrl( environment.getProperty("db.url") );
		dataSource.setUsername( environment.getProperty("db.username") );
		dataSource.setPassword( environment.getProperty("db.password") );
		return dataSource;
	}

}
