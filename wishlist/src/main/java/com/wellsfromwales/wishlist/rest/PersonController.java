package com.wellsfromwales.wishlist.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.wellsfromwales.wishlist.model.Person;
import com.wellsfromwales.wishlist.service.PersonService;

@RestController
@RequestMapping("/rest/person")
public class PersonController {
	
	@Autowired
	private PersonService personService;
	
	@RequestMapping(method=RequestMethod.GET, value="/{id}", produces="application/json")
	public Person findPersonById(@PathVariable("id") Long id) {
		return personService.findPersonById(id);
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/list", produces="application/json")
	public List<Person> listPersons() {
		return personService.listPersons();
	}
	
}
