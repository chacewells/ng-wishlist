package com.wellsfromwales.wishlist.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.wellsfromwales.wishlist.model.Person;

@Repository
public class PersonService {
	
	@PersistenceContext
	private EntityManager entityManager;
	
	public Person findPersonById(Long id) {
		return entityManager.find(Person.class, id);
	}
	
	public List<Person> listPersons() {
		return entityManager
				.createQuery(
						"Select p from Person p", 
						Person.class)
				.getResultList();
	}

}
